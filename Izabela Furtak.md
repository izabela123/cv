# Izabela Furtak - zmieniony plik

Doświadczenie 
- Voice Contact Center Sp. z o.o.
Konsultant infolinii PZU S.A. – rejestracja szkód majątkowych,
osobowych i komunikacyjnych - 08.2016 - obecnie
- Udział w projekcie „Geo4work”
Szkolenia m.in. z zakresu baz danych przestrzennych,
internetowych aplikacji bazodanowych oraz programowania
w języku Java - 12.2017 – 07.2018
- Lubelski Węgiel Bogdanka S.A.
Praktyki zawodowe, Dział Infrastruktury IT – wykonanie
schematu sieci światłowodowej zakładu w programie Bricscad,
kontrola poprawności oraz uzupełnianie bazy danych dotyczącej
sprzętu i aplikacji w LW Bogdanka - 07.2017
- Starostwo Powiatowe w Łęcznej
Praktyki zawodowe, Wydział Architektury i Budownictwa,
Wydział Ochrony Środowiska, Wydział Geodezji, Kartografii,
Katastru i Zarządzania Nieruchomościami – przygotowywanie
wyrysów z ewidencji gruntów i budynków, przygotowanie
zestawienia działek wyłączonych z produkcji rolniczej - 07.2015

Wykształcenie 
- Uniwersytet Marii Curie-Skłodowskiej w Lublinie
Geoinformatyka, studia licencjackie 
2015 - 2018

- Uniwersytet Przyrodniczy w Lublinie
Gospodarka przestrzenna, studia inżynierskie
2012 - 2015

Umiejętności 
- Znajomość relacyjnych baz danych SQL
- Znajomość XML oraz UML
- Podstawy programowania w językach: Python, Java, C++
- Umiejętność obsługi pakietu Microsoft Office
- Umiejętność obsługi programów QGIS, ArcGIS

Języki obce 
- angielski średniozaawansowany

Inne 
- Prawo jazdy kat. B
- Umiejętność pracy w grupie
- Sumienność

Wyrażam zgodę na przetwarzanie danych osobowych zawartych w niniejszym dokumencie do realizacji procesu rekrutacji
zgodnie z ustawą z dnia 10 maja 2018 roku o ochronie danych osobowych (Dz. Ustaw z 2018, poz. 1000) oraz zgodnie
z Rozporządzeniem Parlamentu Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony osób
fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych oraz
uchylenia dyrektywy 95/46/WE (RODO).